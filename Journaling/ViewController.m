//
//  ViewController.m
//  Journaling
//
//  Created by Stuart Adams on 2/2/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView* backgroundImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"oldPaper.png"]];
    backgroundImage.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:backgroundImage];
    
    RootViewController* rvc = [RootViewController new];
    UINavigationController* nc = [[UINavigationController alloc]initWithRootViewController:rvc];
    [self addChildViewController:nc];
    [self.view addSubview:nc.view];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
