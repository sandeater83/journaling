//
//  JournalEntryTableViewCell.h
//  Journaling
//
//  Created by Stuart Adams on 2/4/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalEntryTableViewCell : UITableViewCell
{
    UILabel* titleLbl;
    UILabel* dateLbl;
    //int cellWidth;
}

-(void)setTitleLbl:(NSString*)titleText;
-(void)setDateLabel:(NSString*)dateText;
-(void)setCellWidth:(int)width;

@end


