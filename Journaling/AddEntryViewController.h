//
//  AddEntryViewController.h
//  Journaling
//
//  Created by Stuart Adams on 2/3/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AddEntryViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate>
{
    UITextField* title;
    UITextView* journalEntry;
    AppDelegate* ad;
}
@end
