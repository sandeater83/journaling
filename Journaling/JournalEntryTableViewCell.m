//
//  JournalEntryTableViewCell.m
//  Journaling
//
//  Created by Stuart Adams on 2/4/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "JournalEntryTableViewCell.h"

@implementation JournalEntryTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        dateLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 500, 20)];
        
         titleLbl = [[UILabel alloc]initWithFrame:CGRectMake(10, dateLbl.frame.size.height, 500, 20)];
        
        [self addSubview:titleLbl];
        [self addSubview:dateLbl];
        
        
    }
    return self;
}

-(void)setTitleLbl:(NSString*)titleText
{
    titleLbl.text = titleText;
}

-(void)setDateLabel:(NSString*)dateText
{
    dateLbl.text = dateText;
}

-(void)setCellWidth:(int)width
{
    //cellWidth = width;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
