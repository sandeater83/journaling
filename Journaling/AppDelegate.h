//
//  AppDelegate.h
//  Journaling
//
//  Created by Stuart Adams on 2/2/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSUserDefaults* defaults;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray* journalData;


@end

