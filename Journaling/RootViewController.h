//
//  RootViewController.h
//  Journaling
//
//  Created by Stuart Adams on 2/3/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddEntryViewController.h"
#import "EditEntryViewController.h"
#import "JournalEntryTableViewCell.h"

@interface RootViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray* journal;
    UITableView* journalEnteriesTable;
    AppDelegate* ad;
    int screenWidth;
}

@end
