//
//  RootViewController.m
//  Journaling
//
//  Created by Stuart Adams on 2/3/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "RootViewController.h"
#import "AppDelegate.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Journal Entries";
    
    // Do any additional setup after loading the view.
    
    int screenHeight = self.view.frame.size.height;
    screenWidth = self.view.frame.size.width;
    
    journalEnteriesTable = [[UITableView alloc]initWithFrame:CGRectMake(10, 0, screenWidth - 20, screenHeight - 10)];
    
    journalEnteriesTable.delegate = self;
    journalEnteriesTable.dataSource = self;
    
    UIBarButtonItem* addItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addEntry:)];
    
    NSArray* actionButtonItems = @[addItem];
    
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    journal = ad.journalData;

    NSLog(@"rootview");
    [self.view addSubview:journalEnteriesTable];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return journal.count;
}

-(void)addEntry:(id)sender
{
    NSLog(@"Touched");
    AddEntryViewController* aevc = [AddEntryViewController new];
    [self.navigationController pushViewController:aevc animated:YES];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = @"cell";
    NSDictionary* dict = [journal objectAtIndex:indexPath.row];
    
    JournalEntryTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell)
    {
        cell = [[JournalEntryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [cell setCellWidth:screenWidth - 40];
    
    [cell setTitleLbl:dict[@"title"]];
    [cell setDateLabel:dict[@"date"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    EditEntryViewController* eevc = [[EditEntryViewController alloc]init];
    
    eevc.index = indexPath.row;
    [self.navigationController pushViewController:eevc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    journal = ad.journalData;
    [journalEnteriesTable reloadData];
}

@end
