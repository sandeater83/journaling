//
//  EditEntryViewController.h
//  Journaling
//
//  Created by Stuart Adams on 2/4/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface EditEntryViewController : UIViewController<UITextViewDelegate, UITextFieldDelegate >
{
    AppDelegate* ad;
    NSArray* journal;
    UITextField* title;
    UITextView* journalEntry;
}

@property (nonatomic) int index;

@end
