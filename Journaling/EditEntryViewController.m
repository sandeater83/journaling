//
//  EditEntryViewController.m
//  Journaling
//
//  Created by Stuart Adams on 2/4/15.
//  Copyright (c) 2015 Stuart Adams. All rights reserved.
//

#import "EditEntryViewController.h"

@interface EditEntryViewController ()

@end

@implementation EditEntryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    journal = ad.journalData;
    self.automaticallyAdjustsScrollViewInsets = NO;
    NSDictionary* dict = journal[self.index];
    
    UIBarButtonItem* saveItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveEntry:)];
    
    NSArray* actionButtonItems = @[saveItem];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    journalEntry.delegate = self;
    
    title = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, 30)];
    title.backgroundColor = [UIColor whiteColor];
    title.returnKeyType = UIReturnKeyNext;
    
    journalEntry = [[UITextView alloc]initWithFrame:CGRectMake(15, title.frame.size.height + title.frame.origin.y + 10, self.view.frame.size.width - 30, self.view.frame.size.height - title.frame.size.height - title.frame.origin.y - 100)];
    
    title.text = dict[@"title"];
    self.navigationItem.title = title.text;
    journalEntry.text = dict[@"journalEntry"];
    
    title.delegate = self;
    journalEntry.delegate = self;
    
    [self.view addSubview:journalEntry];
    [self.view addSubview:title];
    
    UIButton* doneBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneBtn setTitle:@"Done Editing" forState:UIControlStateNormal];
    doneBtn.frame = CGRectMake(0, 0, self.view.frame.size.width, 30);
    [doneBtn addTarget:self action:@selector(doneEditing:) forControlEvents:UIControlEventTouchUpInside];
    doneBtn.backgroundColor = [UIColor whiteColor];
    journalEntry.inputAccessoryView = doneBtn;
}

-(void)saveEntry:(id)sender
{
    [ad.journalData[self.index] setValue:title.text forKey:@"title"];
    [ad.journalData[self.index] setValue:journalEntry.text forKey:@"journalEntry"];
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:ad.journalData forKey:@"journalDataArray"];
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, self.view.frame.size.width - 30, self.view.frame.size.height - title.frame.size.height - title.frame.origin.y - 30);
    return true;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self updateTitle];
    [journalEntry becomeFirstResponder];
    return true;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self updateTitle];
    return true;
}

-(void)updateTitle
{
    self.navigationItem.title = title.text;
}

-(void)doneEditing:(id)sender
{
    [self.view endEditing:YES];
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, self.view.frame.size.width - 30, self.view.frame.size.height -320);
    //found this fix on stackOverflow, needed otherwise using the next button set the cursor one line down from the beginning
    [self performSelector:@selector(setCursorPosition:) withObject:textView afterDelay:0.01];
}

-(void)setCursorPosition:(UITextView*)view
{
    journalEntry.selectedRange = NSMakeRange(0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
